import React from "react";

import "./style.scss"

const FlyTypes = ({handleChangeTypes}) => {
    return (
        <div className="types">
            <p className="title">نوع پرواز</p>
            <div className="form">
                <label>
                    <input type="checkbox" name="system" onChange={handleChangeTypes}/>
                    پروازهای سیستمی
                </label>
                <label>
                    <input type="checkbox" name="charter" onChange={handleChangeTypes}/>
                    پروازهای چارتری     
                </label>
            </div>
        </div>
    )
}

export default FlyTypes