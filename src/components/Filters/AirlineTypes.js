import React from "react";

const AirlineTypes = ({handleChangeTypes,airlines}) => {
    return (
        <div className="airlines">
            <p className="title">ایرلاین ها</p>
            <div className="form">
                {airlines?.map((it , index) => (
                    <label key={index}>
                        <input type="checkbox" name={it?.iata} onChange={handleChangeTypes}/>
                        {it.nameFa}
                    </label>
                ))}
            </div>
        </div>
    )
}

export default AirlineTypes;