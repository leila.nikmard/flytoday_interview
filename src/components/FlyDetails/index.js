import React from "react";
import flightIcon from "../../img/flight.png";
import moment from 'jalali-moment';

import "./style.scss";

const FlyDetails = ({data,airLine,flightSegments,arrivalAirportLocationCode,departureAirportLocationCode}) => {
    // const typeOfPerson = 
    return(
        <div className="fly">
            <ul className="tabs">
                <li className="tabs__item"> 
                    <img src={flightIcon} alt="flightIcon" />   
                    جزییات پرواز
                </li>
                <li className="tabs__item">شرایط و قوانین</li>
            </ul>
            <p className="fly__title">پرواز رفت تهران - استانبول</p>
            <div className="fly__details">
                <div className="airline">
                    <div className="airline__logo"></div>
                    <div className="airline__name">{airLine?.name}</div>
                </div>
                <div>
                    <div className="circle"></div>
                    <div className="line"></div>
                    <div className="circle"></div>
                </div>
                <div className="timeline">
                    <div className="arrival">
                        <p className="arrival__time">
                            <span className="time">{
                                moment(flightSegments?.arrivalDateTime)
                                .locale("fa")
                                .format('HH:mm')
                            }</span>
                            <span className="city">{arrivalAirportLocationCode?.cityFa}</span>
                            <span className="aita">({arrivalAirportLocationCode?.iata})</span>
                        </p>
                        <p className="arrival__date">
                            <span className="date__fa">{moment(flightSegments?.arrivalDateTime)
                            .locale('fa')
                            .format('D MMMM YYYY')
                            }{" "}</span>
                            <span className="date__en">({moment(flightSegments?.arrivalDateTime)
                            .format('D MMMM')
                            })</span>
                        </p>
                        <p className="arrival__airport">
                            <span>Imam Khomeini Intl</span>
                            <span></span>
                        </p>
                    </div>
                    <div className="information">
                        <p>
                            <span>مدت پرواز</span>
                            <span></span>
                        </p>
                        <p>
                            <span>نوع هواپیما</span>
                            <span></span>
                        </p>
                        <p>
                            <span>کلاس پرواز</span>
                            <span></span>
                        </p>
                        <p>
                            <span>نوع پرواز</span>
                            <span></span>
                        </p>
                        <p>
                            <span>بار مجاز</span>
                            <span></span>
                        </p>
                        <p>
                            <span>کلاس نرخی</span>
                            <span></span>
                        </p>
                        <p>
                            <span>استرداد</span>
                            <span></span>
                        </p>
                    </div>
                    <div className="departure">
                        <p className="departure__time">
                            <span></span>
                            <span></span>
                        </p>
                        <p className="departure_date">
                            <span></span>
                            <span></span>
                        </p>
                        <p className="departure__airport">
                            <span></span>
                            <span></span>
                        </p>
                    </div>
                </div>
            </div>
            <div className="fly__costs">
                {data?.airItineraryPricingInfo.ptcFareBreakdown.map((it, index) => (
                    <div key={index} className="passenger__cost">
                        <span>{it.passengerTypeQuantity.quantity} x {it.passengerTypeQuantity.passengerType === "Adt" && "بزرگسال"}</span>
                    </div>
                ))}
                <div className="total__costs">
                    <span>مجموع: </span>
                    <span>{data?.airItineraryPricingInfo?.itinTotalFare?.totalFare}</span>
                </div>
                </div>
        </div>
    )
}

export default FlyDetails;