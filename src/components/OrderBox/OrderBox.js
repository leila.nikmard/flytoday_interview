import React from "react";

import "./style.scss"

const OrderBox = ({handleOrderSelect, orderData}) => {
    const options = [
        {id:"", name:""},
        {id:"price", name:"قیمت"},
        {id:"time", name:"زمان"}
    ]
    return(
        <div className="order">
            <select value={orderData} onChange={handleOrderSelect}>
                {options?.map(it => (
                    <option key={it?.id} value={it?.id}>{it?.name}</option>
                ))}
            </select>
        </div>
    )
}

export default OrderBox