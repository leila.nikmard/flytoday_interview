import React from "react";

import "./style.scss"

const Button = ({name}) => {
    return(
        <button className="btn" type="submit">{name}</button>
    )
}

export default Button