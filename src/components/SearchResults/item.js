import React, { useState } from "react";
import FlyDetails from "../FlyDetails";
import moment from 'jalali-moment';
import Button from "../Button";

import "./style.scss";

const SearchResultsItem = ({data , airLines,airPorts}) => {
  const [showDetails , setShowDetails] = useState(false);
  const flightSegments = data?.originDestinationOptions[0]?.flightSegments[0];
  const numberOfPerson = data?.airItineraryPricingInfo.ptcFareBreakdown.reduce((total,a) => {
    return total + a.passengerTypeQuantity.quantity
  },0);
  const totalFare = data.airItineraryPricingInfo.itinTotalFare.totalFare;
  
  
  const airLine = airLines.find((item) => item.iata === data?.validatingAirlineCode);
  const arrivalAirportLocationCode = airPorts?.find((item) => item?.iata === flightSegments?.arrivalAirportLocationCode);
  const departureAirportLocationCode = airPorts.find((item) => item?.iata === flightSegments?.departureAirportLocationCode);
  const handleShowDetails = () => {
    setShowDetails(!showDetails)
  }
  
  return (
    <div className="card">
      <div className="card__information">
        <div className="card__airLine">
          <div className="card__airLine__logo"></div>
          <div className="card__airLine__name">{airLine?.nameFa}</div>
        </div>
        <div className="card__timeLine">
          <div className="departure">
          <div className="departure__time">{
            moment(flightSegments?.arrivalDateTime)
            .locale("fa")
            .format('HH:mm')
            }</div>
            <div className="departure__airport">
              <span className="airport__city">{arrivalAirportLocationCode?.cityFa}</span>
              <span className="airport__aita">({arrivalAirportLocationCode?.iata})</span>
            </div>
          </div>
          <div className="length">
            <div className="circle"></div>
            <div className="line"></div>
            <div className="circle"></div>
          </div>
          <div className="arrival">
          <div className="arrival__time">{moment(flightSegments?.departureDateTime)
            .locale("fa")
            .format('HH:mm')
            }</div>
            <div className="arrival__airport">
              <span className="airport__city">{departureAirportLocationCode?.cityFa}</span>
              <span className="airport__aita">({departureAirportLocationCode?.iata})</span>
            </div>
          </div>
        </div>
        <div className="card__price">
          <p className="number">{numberOfPerson} نفر</p>
          <p className="price">
            <span className="price__amount">{totalFare}</span>
            <span className="price__currency">تومان</span>
          </p>
          <Button name="انتخاب بلیط"/>
        </div>
      </div>
      <div className="card__details">
        <div className="details">
          <p className="type">
            {flightSegments?.isCharter ? "چارتر" : "سیستمی"}
          </p>
          <p className="fly__class">اکونومی</p>
          <p className="seats__remaining">
            <span>{flightSegments?.seatsRemaining}</span>
            <span>صندلی خالی</span>
          </p>
          <p className="flight__number">
            <span>شماره پرواز: </span>
            <span>{flightSegments?.flightNumber}</span>
          </p>
          <p className="supplier">
            <span>تامین کننده: </span>
            <span>پرایس لاین</span>
          </p>
        </div>
        <div className="more__details" onClick={handleShowDetails}>جزئیات بیشتر</div>
      </div>
      {showDetails && 
      <FlyDetails 
        airLine={airLine} 
        data={data}
        flightSegments={flightSegments}
        arrivalAirportLocationCode={arrivalAirportLocationCode}
        departureAirportLocationCode={departureAirportLocationCode}
      />}
    </div>
  );
};

export default SearchResultsItem;
