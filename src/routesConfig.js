import SearchResults from "./Pages/SearchResults"

const routesConfig = [
    {
        path:"/",
        component: SearchResults
    },
];

export default routesConfig;

