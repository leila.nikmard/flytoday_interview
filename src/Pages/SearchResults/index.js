import React, { useEffect, useState } from "react";
import SearchResultsItem from "../../components/SearchResults/item"
import flightData from "../../local-json/flight-data.json";
import FlyTypes from "../../components/Filters/FlyTypes";
import AirlineTypes from "../../components/Filters/AirlineTypes";
import OrderBox from "../../components/OrderBox/OrderBox";

import "./style.scss"

const SearchResults = () => {
    const [flightList , setFlightList] = useState(flightData?.pricedItineraries);
    const [airLinesData , setAirLinesData] = useState(flightData?.additionalData?.airlines);
    const [airPortsData , setAirPortsData] = useState(flightData?.additionalData?.airports);
    const [flyType , setFlyType] = useState({
        "charter":false,
        "system": false
    });
    const [airlinesType , setAirlinesType]= useState([]);
    const [order , setOrder] = useState("")

    const handleChangeFlyTypes = (e) => {
        const name = e.target.name;
        setFlyType({...flyType , [name]:!flyType[name]});
    };

    const handleChangeAirlineTypes = (e) => {
        const name = e.target.name;
        setAirlinesType([...airlinesType , name])
    }

    const handleOrderSelect = (e) => {
        const value  = e.target.value;

        const data = value === "price" ? flightList.sort((p1 , p2) => (p1.airItineraryPricingInfo.itinTotalFare?.totalFare < p2.airItineraryPricingInfo.itinTotalFare?.totalFare) ? 1 : (p1.airItineraryPricingInfo.itinTotalFare?.totalFare > p2.airItineraryPricingInfo.itinTotalFare?.totalFare) ? -1 : 0) : 
       flightList.sort((p1 , p2) => (p1.originDestinationOptions[0].flightSegments[0].journeyDurationPerMinute < p2.originDestinationOptions[0].flightSegments[0].journeyDurationPerMinute) ? 1 : (p1.originDestinationOptions[0].flightSegments[0].journeyDurationPerMinute > p2.originDestinationOptions[0].flightSegments[0].journeyDurationPerMinute) ? -1 : 0);
       setOrder(value);
       setFlightList(data);  
    }

    useEffect(() => {
        const charterData = flightData.pricedItineraries.filter((it) => it?.originDestinationOptions[0]?.flightSegments[0].isCharter);

        const systemData = flightData.pricedItineraries.filter((it) => !(it?.originDestinationOptions[0]?.flightSegments[0].isCharter));

        if(flyType["charter"] && !flyType["system"]){
            setFlightList(charterData)
        }else if(flyType["system"] && !flyType["charter"]){
            setFlightList(systemData)
        }else if (flyType["system"] && flyType["charter"] || !flyType["system"] && !flyType["charter"]){
            setFlightList([...charterData , ...systemData])
        }
        
    },[flyType]);

    useEffect(() => {
        const data = flightData.pricedItineraries.filter((it) => airlinesType.map(item => it?.validatingAirlineCode === item));

        setFlightList(data)
    },[airlinesType]);

    return (
        <div className="container">
            <div className="result">
            <OrderBox 
                orderData={order}
                handleOrderSelect={handleOrderSelect} 
            />
                {flightList.length > 0 ? flightList?.map((it,index) => (
                    <div key={index}>
                        <SearchResultsItem 
                            data={it} 
                            airPorts={airPortsData} 
                            airLines={airLinesData}
                        />
                    </div>
                )) : <p>نتیجه ای یافت نشد</p>}
            </div>
            <div className="filter">
                <FlyTypes 
                    handleChangeTypes={handleChangeFlyTypes}
                />
                <AirlineTypes 
                    airlines={airLinesData} 
                    handleChangeTypes={handleChangeAirlineTypes}
                />
            </div>
        </div>
    )
}

export default SearchResults;