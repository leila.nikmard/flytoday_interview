import React from "react";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import history from "./utils/helpers/history";
import routesConfig from "./routesConfig"

const RouterComponent = () => {
  return (
    <Router history={history}>
      <Switch>
        {routesConfig.map((it,index) => <Route key={index} path={it.path} exact component={it.component} />)}
      </Switch>
    </Router>
  );
};
export default RouterComponent;
