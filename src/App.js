import React from "react";
import RouterComponent from "./routes";
import "./index.css";

function App() {
  return (
    <RouterComponent />
  );
}

export default App;
